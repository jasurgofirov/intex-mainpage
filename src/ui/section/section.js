import React, { useState } from "react";
import "./section.css";
import Modals from "./Modal";

const Section = () => {
  const [modalActive, setModalActive] = useState(false);

  return (
    <section>
      <div className="SectionContainer">
        <div className="delivery">
            <h1>Tekinga yetkazib berish</h1>        
              <p>
                Toshkent shahar ichida yetkazib berish bepul(shahar tashqarisida
                yetkazib berish alohida to'lanadi)    
              </p>
            <button className="delivery_btn" onClick={() => setModalActive(true)}>
              Buyurtma berish
            </button>
        {     modalActive && <Modals closeModal={setModalActive} />}
        </div>

        <div className="section_title">
          <h1>Mijozlarni qadrlash</h1>
        </div>

        <div className="section_boxes">
          <div className="box">
            <div className="box_img">
              <img src="./image/sectionEmoji.svg" alt='img'/>
            </div>
            <div className="box_content">
              <h1>Tajriba</h1>
              <p>Xodimlarimizning profisionalligi</p>
            </div>
          </div>

          <div className="box">
            <div className="box_img">
              <img src="./image/sectionEmoji2.svg"  alt='img' />
            </div>
            <div className="box_content">
              <h1>Yetkazib berish</h1>
              <p>Shahar ichida bepul yetkazib berish</p>
            </div>
          </div>

          <div className="box">
            <div className="box_img">
              <img src="./image/sectionEmoji3.svg" alt='img' />
            </div>
            <div className="box_content">
              <h1>Sifat</h1>
              <p>Mustahkam sifatli baseynlar</p>
            </div>
          </div>
        </div>

        <div className="section_title">
          <h1>Intex baseynlari Toshkentda</h1>
        </div>

        <div className="section_detail">
          <div className="detail">
            <p>
              Intex basseynlari - bu butun oila uchun yoqimli dam olish uchun
              mo'ljallangan arzon, yuqori sifatli, ishonchli va ekologik toza
              mahsulotlar. Basseyn har qanday hovliga to'liq o'rnatilishi va yozda
              faol foydalanilishi mumkin. Basseyn sizga yorqin his-tuyg`ularni
              beradi va issiq yoz kunlarida sizni jaziramadan qutqaradi.
            </p>
          </div>

          <div className="detail">
            <p>
              Intex Basseynlari afzalliklarning kengligi bilan ajralib turadi,
              quyida ulardan eng muhimlarini ajratib ko'rsatish mumkin:
            </p>
            <ul>
                <li>
                 <i className="fa-solid fa-circle-check"></i>
                  <p>Chidam</p>
                </li>
            </ul>
            <ul>
                <li>
                 <i className="fa-solid fa-circle-check"></i>
                  <p>O'rnatish uchun arzon</p>
                </li>
            </ul>
            <ul>
                <li>                  
                 <i className="fa-solid fa-circle-check"></i>
                  <p>Chiroyli va yorqin ranglar</p>
                </li>
            </ul>
            <ul>
                <li>                  
                 <i className="fa-solid fa-circle-check"></i>
                  <p>Zamonaviy dizayn</p>
                </li>
            </ul>
            <ul>
              <li>
                 <i className="fa-solid fa-circle-check"></i>
                  <p>Yuqori sifat</p>
                </li>
            </ul>
          </div>
        </div>
      </div>
    </section>
  );
};

export default Section;
