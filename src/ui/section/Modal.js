import React, { useState } from "react";
import "../section/Modal.css";
import {FcAssistant} from 'react-icons/fc'
import {GrClose} from 'react-icons/gr'
import axios from "../../api/axios";

const KONSULTATSIYAURL = "konsultatsia/";

function Modals({closeModal}) {
  const [name, setName]=useState("")
  const [number, setNumber]=useState("")
  
  const handleSumbit = async (e) => {

    e.preventDefault();
    try {
      const token = JSON.parse(window.localStorage.getItem("AuthToken")).access;
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };

      const res = await axios.post(
        KONSULTATSIYAURL,
        {name: name, phone:number, active: false },
        config
        );
        console.log(res);
    } catch (error) {
      console.log(error);
    }
    closeModal(false)

  };

  
  return (
    <>
      <div className="modalWrapper">
          <div className="modal_contents">
            <div className="modal_contents_header">
              <div className="navbarModal">
                <span className="close_span">  <GrClose className="close_icon" size="25px"  onClick={()=>closeModal(false)}/></span>
              </div>
              <FcAssistant  size="140px" />
              <h2 className="Consultation_title">Konsultatsiya olish</h2>
            </div>  
            <form className="form" onSubmit={handleSumbit}>
              <input type="text" id="email"  className="form__input" autocomplete="off" placeholder="Ismingiz" value={name} onChange={(e)=>setName(e.target.value)}/>              
              <input type="text" id="email"  className="form__input" autocomplete="off" placeholder="Raqamingiz" value={number} onChange={(e)=>setNumber(e.target.value)}/>  
              <input type="submit" className="btn_buyurtmaModal"  value={"Buyurtma berish"}  />            
            </form>
          </div>
      </div>
    </>
  );
}
export default Modals;
