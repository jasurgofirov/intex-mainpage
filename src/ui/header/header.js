import "./header.css";
// import "../MediaQuarey/MediaQuarey.css";
import { useSelector } from "react-redux";
import {HashLink as Link} from "react-router-hash-link";  
import Sidebar from './sidebar';

const Header = () => {
  const state = useSelector((state) => state);

  return (
    <header>
      <div className="headerContainer">
        {/* <div class="menu-btn"></div> */}
         <Sidebar />
      <div className="navbar">
          <h1><Link to="/" className="Link">INTEX-MARKET.UZ</Link></h1>
            <ul className="nav_ul">
              {state.data.map((item) => {
                return (
                  <li key={item.id} className="nav_li" > 
                  <Link smooth to={`#${item.categoryname}`}className="Link">
                    <span className="span_li">
                        {item.categoryname}
                    </span>
                    </Link>
                  </li>
                );
              })}
            </ul> 

            <div className="navbar_media">
              <a href='#' className='phone_number'>{state.info?.tel}</a>
              <a href={state.info?.telegram} target="_blank">
                <img className="header_img" src="./image/navbarImg1.svg" />
              </a>
              <a href={state.info?.instagram} target="_blank">
                <img className="header_img1" src="./image/navbarImg2.svg" />
              </a>
              <span className="language">UZ</span> 
            </div>
          </div>
      </div>
    </header>
  );
};

export default Header;
