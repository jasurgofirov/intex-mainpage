
import React from 'react';
import { slide as Menu } from 'react-burger-menu';
import { useSelector } from "react-redux";
import {HashLink as Link} from "react-router-hash-link";  

const SideBarModal = () => {
  const state = useSelector((state) => state);
  return (
    <Menu>
      <a className="menu-item" href="/">INTEX-MARKET.UZ</a>
      <ul className="menu-ul">
      {state.data.map((item) => {
              return (
                <li key={item.id} className="menu_li">
                  <Link smooth to={`#${item.categoryname}`} className="menu-Link">
                      {item.categoryname}
                  </Link>
                </li>
              )
            })}
      </ul> 

      <div className='menu-btn menu-call'>
      <i className="fa-solid fa-square-phone"></i> <p>Call</p>
      </div>
      <div className='menu-btn menu-tme'>
      <i className="fa-brands fa-telegram"></i> <p>Telegram</p>
      </div>
      <div className='menu-btn menu-ins'>
      <i className="fa-brands fa-instagram-square"></i> <p>Instagram</p>
      </div>
    </Menu>
    )
}

export default SideBarModal; 