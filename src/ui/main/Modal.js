import React, { useState } from "react";
import { GrClose } from "react-icons/gr";
import "../main/Modal.css";
import { useSelector } from "react-redux";
import axios from "axios";

const ORDERURL = "zakaz/";

function Modal({ closeModal, id }) {
  const state = useSelector((state) => state);

  const [name, setName] = useState("");
  const [phone, setPhone] = useState("");
  const [location, setLocation] = useState("");

  const handleSumbit = async (e) => {
    e.preventDefault();
    try {
      const token = JSON.parse(window.localStorage.getItem("AuthToken")).access;
      const config = {
        headers: { Authorization: `Bearer ${token}` },
      };
      const res = await axios.post(
        ORDERURL,
        { name: name, 
          phone: phone, 
          product_name:id,
          pool_frame:null,
          money:null,
          address: location, 
          active: false, 
          count: 0, },
        config
        );
        console.log(res);
    } catch (error) {
      console.log(error);
    }
    closeModal(false)
  };

  return (  
      <div className="modalContainer">
        <div className="modal_content">
          <div className="m_cloce">
                <GrClose
                  className="close_icon"
                  size="25px"
                  onClick={() => closeModal(false)}
                />
          </div>
          <form className="modal_form" onSubmit={handleSumbit}>
            <div className="modal_form_img">
              {state.product.map((item) => {
                if (item.id === id) {
                  return <img src={item.image} key={item.id} alt='img' />;
                }
              })}
              <br />
              {state.product.map((item) => {
                if (item.id === id) {
                  return <p key={item.id}>{item.old_price} so'm</p>;
                }
              })}
            </div>
            <div className="modal_form_value">
              <input
                type="text"
                className="input_text"
                placeholder="Ismingiz"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
              <input
                type="phone"
                className="input_text"
                placeholder="Telefon raqamingiz"
                value={phone}
                onChange={(e) => setPhone(e.target.value)}
              />
              <input
                type="text"
                className="input_text"
                placeholder="Manzil"
                value={location}
                onChange={(e) => setLocation(e.target.value)}
              />
              <input type="submit" value={"Buyurtma berish"}  className="form_order_btn"/>
            </div>
          </form>
        </div>
      </div>
  );
}
export default Modal;
