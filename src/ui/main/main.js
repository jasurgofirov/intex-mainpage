import React, { useState } from "react";
import "./main.css";
import { useSelector } from "react-redux";
import Modals from "./Modal";

const Main = (props) => {
  const state = useSelector((state) => state);
  const [modalActive, setModalActive] = useState(false);
  const [id, setId] = useState("");

    const handleOpen =(id)=>{
      setModalActive(true);
      setId(id)
    }

  return (
    <section>
        <div className="mainContainer">
          {state.data.map((item) => { 
            return (
                <div className="Category_content" key={item.id}>
                  <div className="Category_title">
                    <h1 id={`${item.categoryname}`}>
                      {item.categoryname}
                    </h1>
                  </div>
                  <div className="Product_content">
                    {state.product.map((i) => {
                      if (item.id === i.category) {
                        return (
                          <div className="product" key={i.id}>
                            <div>
                            <button
                            className={
                                  i.recommendation_uz === "Tavsiya qilamiz" ? "product_btn btn_recomend"
                                    : i.recommendation_uz === "Chegirma" ? "product_btn btn_discount" 
                                    :i.recommendation_uz === "Yo'q" ? "btnNone"  :  "product_btn btn_not_sold"
                            }
                          >
                            {i.recommendation_uz}
                             </button>
                            </div>
                            <div className="type_product">
                                  <span>{}</span>
                            </div>
                            <div className="product_img">
                              <img src={i.image} className="mainImg" />
                            </div>
                            <div className="product_value">
                              <div className='p_value'>
                                <span className="del">{i?.old_price} <div className="line"></div></span>
                               <p><b> {i.dis_price} so'm </b></p>
                              </div>
                              <div className="p_btn">
                              <button
                                className="order_btn"
                                onClick={() => handleOpen(i.id)}
                              >
                                Buyurtma qilish
                              </button>
                              </div>
                              {modalActive && (
                                <Modals closeModal={setModalActive} id={id}  />
                              )}
                            </div>
                          </div>
                        );
                      }
                    })}
                  </div>
                </div>
            );
          })}
        </div>
    </section>
  );
};

export default Main;
