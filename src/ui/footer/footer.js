import React, { useState } from "react";
import { useSelector } from "react-redux";
import "./footer.css";
import axios from "../../api/axios";

const CONS_URL = "konsultatsia/";

function Footer() {
   const state = useSelector((state) => state);
   const [modalActive, setModalActive] = useState(false);
   
  const [name, setName]=useState("")
  const [number, setNumber]=useState("")

   const handleSumbit = async (e) => {
      e.preventDefault();
      try {
        const token = JSON.parse(window.localStorage.getItem("AuthToken")).access;
        const config = {
          headers: { Authorization: `Bearer ${token}` },
        };
  
        const res = await axios.post(
         CONS_URL,
          {name: name, phone:number, active: false },
          config
          );
          console.log(res);
      } catch (error) {
        console.log(error);
      }
      setName("")
      setNumber("")
  
    };

   return (
      <footer>
            <div className="footer_container">       
                  <form className="footer_form" onSubmit={handleSumbit}>
                  <h1>Bepul konsultatsiya yordami uchun</h1>
                  <input type="text" placeholder="Ismingiz" className="footer_input" value={name} onChange={(e)=>setName(e.target.value)}/>
                  <input type="phone" placeholder="Telefon raqamingiz" className="footer_input" value={number} onChange={(e)=>setNumber(e.target.value)}/>
                  <input type="submit" value={"Konsultatsiya olish"} className="footer_btn" />
            
                  </form>   
                  <div className="footer_container_2">
                     <div className="working">
                     <i className="fa-solid fa-clock"></i>
                     <p>Ish vaqti</p>
                     </div>
                     <div className="working_days">
                     <p>{state.info?.time_uz}</p>
                     </div>
                     <div className="footer_links">
                        <a href={state.info?.tel} target='_blank' >
                           <img className="footerImgIcon1" src="./image/telegram.svg" alt='img' />
                        </a>
                           <a href={state.info?.telegram} target='_blank'>
                                 <img className="footerImgIcon2" src="./image/navbarImg1.svg" alt='img' />
                        </a>
                        <a href={state.info?.instagram} target='_blank'>
                                 <img className="footerImgIcon3" src="./image/navbarImg2.svg" alt='img' />
                        </a>
                     </div>
                  </div>
                  <div className="footer_container_3">
                     <div className="footer_adress">
                     <p>Internet-market.uz</p>
                     <p>{state.info?.tel} </p>
                     <p>{state.info?.address_uz}</p>
                     </div>
                     <div className="footer_adress">
                        <p>Разработано в Support Solutions</p> 
                        <p>Все права защищены.</p>
                     </div>
                  </div>
            </div>
      </footer>
   );
}
export default Footer;
