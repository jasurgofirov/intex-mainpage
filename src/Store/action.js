export const add_category = (data) => {
   return {
      type: "ADD_CATEGORY",
      data,
   };
};

export const web_data = (info) => {
   return {
      type: "WEB_DATA",
      info,
   };
};

export const product_data = (product) => {
   return {
      type: "PRODUCT_DATA",
      product,
   };
};

// console.log(add_category())
