import { type } from "@testing-library/user-event/dist/type";

export const AddCategory = (state = { data: [], info: [], product: [] }, action) => {
   if (action.type === "ADD_CATEGORY") {
      return {
         ...state,
         ...action,
      };
   }  else if (action.type === "WEB_DATA") {
      return {
         ...state,
         info: action.info[0],
      };
   } else if (action.type === "PRODUCT_DATA") {
      return {
         ...state,
         product: action.product,
      };
   } 

   return state;
};
