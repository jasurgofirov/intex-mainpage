import { useEffect } from 'react';
import './App.css';
import axios from 'axios';
import { useDispatch, useSelector } from 'react-redux';
import {add_category, web_data, product_data} from './Store/action';
import Header from './ui/header/header';
import Main from './ui/main/main';
import Section from './ui/section/section'
import Footer from './ui/footer/footer'
import axiosInstance from './api/axiosInstance';
const baseURL = 'https://mamirovs.pythonanywhere.com'
const LOGIN_URL = "/token/";


const CATEGORY_URL = '/category/';
const WEB_URL = '/site/';
const PRODUCT_URL = '/product/';

function App() {

  const state = useSelector(state => state);
  const dispatch = useDispatch();
 
  const getData = async (url, action) => {
    const response = await axios.post(`${baseURL}${LOGIN_URL}`, JSON.stringify({ username: "admin", password: "admin" }), {
            headers: { "Content-Type": "application/json" },
         }
         );
         localStorage.setItem("AuthToken", JSON.stringify(response.data));

    const tokenGet = JSON.parse(window.localStorage.getItem('AuthToken'))?.access;
    axiosInstance.get(`${baseURL}${url}`,
          {headers: {'Authorization': `Bearer ${tokenGet}`}},
    ).then((res) => {
      // console.log(res)  
      dispatch(action(res.data));
    }).catch((err)=>{
      console.log(err);
      return err;
    })
  }

  useEffect( () => {
    getData(CATEGORY_URL,add_category);
    getData(PRODUCT_URL,product_data);
    getData(WEB_URL,web_data);

      },[])
      
      console.log(state);
    return (
      <>
         <Header />
         <Main />
         <Section />
         <Footer />
      </>
  );
}

export default App;
